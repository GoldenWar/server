/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import java.rmi.*;
import java.net.InetAddress;
import java.rmi.registry.LocateRegistry;

public class MainMine {
    public static void main(String[] args) {
        try {
            LocateRegistry.createRegistry(1099);
            System.out.println("Mise en place du Security Manager ...");
            for(int i = 1; i<6; i++) {
                Mine mine = new Mine(i);
                String url = "rmi://" + InetAddress.getLocalHost().getHostAddress() + "/Mine/" + mine.getIdMine();
                System.out.println("Enregistrement de l'objet avec l'url : " + url);
                Naming.rebind(url, mine);
            }
            System.out.println("Serveur lancé");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}