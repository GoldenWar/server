/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import java.util.UUID;

public class MiningObject {
    private UUID idMiner;
    private Integer idMine;
    private String idUnit;
    private int gold;

    public MiningObject(){}

    public MiningObject(UUID idMiner, int idMine, String idUnit, int gold) {
        this.idMiner = idMiner;
        this.idMine = idMine;
        this.idUnit = idUnit;
        this.gold = gold;
    }

    public UUID getIdMiner() {
        return idMiner;
    }

    public Integer getIdMine() {
        return idMine;
    }

    public String getIdUnit() {
        return idUnit;
    }

    public int getGold() {
        return gold;
    }
}
