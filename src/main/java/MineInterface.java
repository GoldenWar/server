/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MineInterface extends java.rmi.Remote {

    public Integer mineGold() throws RemoteException, InterruptedException;
}