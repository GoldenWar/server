/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import com.corundumstudio.socketio.SocketIOServer;
import java.beans.Expression;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Game {
    SocketIOServer server;
    HashMap<UUID,Player> players; // matching players / UUID of client's socket

    public Game(SocketIOServer server) {
        this.server = server;
        this.players = new HashMap<UUID, Player>();
    }

    // return true if player have been added, false either.
    public Boolean addPlayer(UUID client, String name) {
        if(this.players.keySet().size() < 4 ) {
            this.players.put(client,new Player(client,name,server,this));
            return true;
        }
        return false;
    }

    public void attack(UUID attacker, String idUnit, Player target) {
        try {
            Player playerAttacker = this.players.get(attacker);
            if(playerAttacker.canAttack(target)) {
                playerAttacker.attack(idUnit,target);
            } else {
                System.out.println("ATTACK IMPOSSIBLE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removePlayer(UUID client){
        this.players.remove(client);
    }

    public boolean useMine(UUID idPlayer,String idUnit, int idMine) {
        try {
            Player player = this.players.get(idPlayer);
            if(player.canWork()) {
                player.mine(idUnit,idMine);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  false;
    }

    public boolean isGameReady() {
        if(this.players.size() == 4){
            return true;
        }
        return false;
    }

    public boolean isFinished() {
        int cpt=0;
        for (Player p: this.players.values()) {
            if(p.isAlive) {
                cpt+=1;
            }
        }
        if(cpt>1) {
            return false;
        }
        return true;
    }

    public Player getWinner() {
        for (Player p: this.players.values()) {
            if(p.isAlive) {
                return p;
            }
        }
        return null;
    }

    public Player getPlayer(UUID idTarget) {
        return players.get(idTarget);
    }
}
