/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.ArrayBlockingQueue;

public class Mine extends UnicastRemoteObject implements MineInterface {
    private boolean available;
    private ArrayBlockingQueue<Mine> set = new ArrayBlockingQueue<Mine>(100);
    private int idMine;

    public Mine(int number) throws RemoteException {
        super();
        this.idMine = number;
        this.available = true;
    }

    public int getIdMine() {
        return idMine;
    }

    public synchronized Integer mineGold() throws RemoteException, InterruptedException {
        System.out.println("ordre minage");
        set.add(this);
        System.out.println("Array: " + set);
        System.out.println("Number of elements in the array = " + set.size());
        while(!available) {
            this.wait();
        }
        available = false;
        System.out.println("début minage");
        this.wait(10000);
        this.available=true;
        System.out.println("fin minage mine"+this.idMine);
        System.out.println("Number of elements in the set = " + set.size());
        set.remove(this);
        if(set.peek() != null) {
            set.peek().notify();
        }
        return 200;
    }
}