/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import com.corundumstudio.socketio.SocketIOServer;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Player { //Classe joueurs (4 instances max), contient x threadsunités
    private final Game game;
    private String name;
    private UUID id;
    private int gold;
    private int health;
    private ArrayList<Unit> units;
    boolean isAlive;
    public boolean isAttacking;
    public AtomicBoolean isAttacked;
    public AtomicBoolean isMining;
    final Object lockAttack = new Object();
    final Object lockMine = new Object();
    ArrayBlockingQueue<Unit> set = new ArrayBlockingQueue<Unit>(100);
    SocketIOServer server;

    public Player(UUID id, String name, SocketIOServer server,Game g) {
        this.id = id;
        this.health = 1000;
        this.gold = 200;
        this.isAlive = true;
        this.game = g;
        this.name = name;
        this.isMining = new AtomicBoolean(false);
        this.isAttacked = new AtomicBoolean(false);
        this.server = server;
        this.units = new ArrayList<Unit>();
        for(int i = 0; i < 3; i++) {
            this.units.add(new Unit(this,this.name+'_'+i)); //ici on instancie juste un runnable, et quand l'unité doit faire une action, on lance le thread
        }
    }

    public void  attack(String idUnit, Player target) throws InterruptedException {
        boolean freeUnit = false;
        for(Unit unit : this.units) {
            System.out.println("Player : "+this.id+" Unit : "+unit.getMode()+" FREE : "+freeUnit+" ID : "+unit.getId()+" IDUNITPARAMETER : "+idUnit);
            if(unit.getMode().equals("passif") && !freeUnit && unit.getId().equals(idUnit)) {
                freeUnit = true;
                unit.setActualTarget(target);
                unit.setMode("attack");
                Thread t = new Thread(unit);
                t.start();
            }
        }
    }

    public void mine(String idUnit, int idMine) {
        boolean freeUnit = false;
        for(Unit unit : units) {
            if(unit.getMode().equals("passif") && !freeUnit && unit.getId().equals(idUnit)) {
                freeUnit = true;
                unit.setActualMineTarget(idMine);
                unit.setMode("mine");
                Thread t = new Thread(unit);
                System.out.println("THREAD CREE FOR MINING");
                t.start();
            }
        }
    }

    public boolean canAttack(Player target) {
        if(target.isAlive && this.isAlive) {
            return canWork();
        }
        return false;
    }

    public boolean canWork() {
        for(Unit u : this.units) {
          if(u.getMode() == "passif") {
                    return true;
           }
        }
        return false;
    }

    public boolean unitHasFinish(String idUnit){
        for (Unit u : this.units) {
            if(u.getId() == idUnit) {
                if(u.getMode() == "passif") {
                    return true;
                }
            }
        }
        return false;
    }

    public UUID getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void looseHealth(double health) {
        this.health-=health;
        if(this.health <= 0) {
            server.getBroadcastOperations().sendEvent("playerDie", this.id);
            this.isAlive = false;
            if(this.game.isFinished()) {
                server.getBroadcastOperations().sendEvent("gameEnd", this.game.getWinner().getId());
            }
        }
    }

    public AtomicBoolean isMining() {
        return isMining;
    }

    public void setMining(AtomicBoolean mining) {
        isMining = mining;
    }

    public void winGold(int gold) { this.gold += gold; }

    public int getGold() { return gold; }
}
