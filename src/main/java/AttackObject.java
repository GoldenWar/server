/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
* Master 2 Informatique ALMA
* Middleware 2020-2021*/

import java.util.UUID;

public class AttackObject {
    private String idAttacker;
    private String idTarget;
    private String idUnit;
    private int targetHpLost;
    private int attackerGoldLoose;

    public AttackObject() {}

    public AttackObject(String idTarget, String idUnit, String idAttacker, int targetHpLost, int attackerGoldLoose) {
        super();
        this.idTarget = idTarget;
        this.idUnit = idUnit;
        this.idAttacker = idAttacker;
        this.targetHpLost = targetHpLost;
        this.attackerGoldLoose = attackerGoldLoose;
    }

    public AttackObject(String idTarget, String idUnit, String idAttacker) {
        this(idTarget,idUnit,idAttacker,0,0);
    }

    public String getIdUnit() {
        return idUnit;
    }

    public void setIdUnit(String idUnit) {
        this.idUnit = idUnit;
    }

    public String getIdTarget() {
        return idTarget;
    }

    public void setIdTarget(String idTarget) {
        this.idTarget = idTarget;
    }

    public String getIdAttacker() {
        return idAttacker;
    }

    public void setIdAttacker(String idAttacker) {
        this.idAttacker = idAttacker;
    }

    public int getTargetHpLost() {
        return targetHpLost;
    }

    public void setTargetHpLost(int targetHpLost) {
        this.targetHpLost = targetHpLost;
    }

    public int getAttackerGoldLoose() {
        return attackerGoldLoose;
    }

    public void setAttackerGoldLoose(int attackerGoldLoose) {
        this.attackerGoldLoose = attackerGoldLoose;
    }
}
