/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import com.corundumstudio.socketio.listener.*;
import com.corundumstudio.socketio.*;
import java.util.HashMap;
import java.util.UUID;

public class Server {
    public static void main(String[] args)throws InterruptedException {
        //Les fonctions pour la communication avec les clients (sockets).

        Configuration config = new Configuration();
        config.setHostname("localhost");
        config.setPort(9092);

        final SocketIOServer server = new SocketIOServer(config);
        final Game game = new Game(server);

        server.addConnectListener(new ConnectListener() {
            public void onConnect(SocketIOClient client) {
                if(!game.addPlayer(client.getSessionId(),"player"+game.players.values().size())) {
                    client.sendEvent("gameFull");
                } else {
                    HashMap<String,UUID> playersInfo = new HashMap<String,UUID>();
                    for (Player p : game.players.values()) {
                        playersInfo.put(p.getName(),p.getId());
                    }
                    client.sendEvent("gameJoined",game.players.get(client.getSessionId())); // event for the client with his num of player
                    server.getBroadcastOperations().sendEvent("refreshPlayersInfo",playersInfo); // we send to all , the information that a client have joined
                    if(game.isGameReady()) {
                        server.getBroadcastOperations().sendEvent("gameReady");
                    }
                }
            }
        });

        server.addDisconnectListener(new DisconnectListener() {
            public void onDisconnect(SocketIOClient socketIOClient) {
                game.removePlayer(socketIOClient.getSessionId());
                HashMap<UUID,String> playersInfo = new HashMap<UUID,String>();
                for (Player p : game.players.values()) {
                    playersInfo.put(p.getId(),p.getName());
                }
                server.getBroadcastOperations().sendEvent("refreshPlayersInfo",playersInfo);
            }
        });

        server.addEventListener("unitOnMine", MiningObject.class, new DataListener<MiningObject>() {
            @Override
            public void onData(SocketIOClient client, MiningObject data, AckRequest ackRequest) {
                // ici on tente de mettre une unite sur une mine et on renvoie si oui ou non c'est okay
                System.out.println("mining will maybe start");
                if(game.useMine(data.getIdMiner(),data.getIdUnit(),data.getIdMine())) {
                    server.getBroadcastOperations().sendEvent("mineStarted", data);
                } else {
                    client.sendEvent("echecMine",data);
                }
            }
        });

        server.addEventListener("attack", AttackObject.class, new DataListener<AttackObject>() {
            @Override
            public void onData(SocketIOClient client, AttackObject data, AckRequest ackRequest) {
                // ici on tente d'attaquer
                data.setAttackerGoldLoose(50);
                server.getBroadcastOperations().sendEvent("attackStarted", data);
                Player p = game.getPlayer(UUID.fromString(data.getIdTarget()));
                String id = data.getIdUnit();
                game.attack(client.getSessionId(),id,p);
            }
        });

        server.start();
        Thread.sleep(Integer.MAX_VALUE);
        server.stop();
    }
}
