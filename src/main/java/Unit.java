/* GOLDENWAR projet réalisé par : Erwan DURAND, Lucie GIRARD, Mathis LAURENT et Tristan RACAPE
 * Master 2 Informatique ALMA
 * Middleware 2020-2021*/

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import static java.lang.Thread.sleep;

public class Unit implements Runnable {
    //implemente runnable, gère une seule unité.
    private String id;
    private Player player;
    private Player actualTarget;
    private int actualMineTarget;
    private String mode; // attack, mine, passif

    public Unit(Player player, String id){
        this.player = player;
        this.id = id;
        this.mode = "passif";
        this.actualTarget = null;
        this.actualMineTarget = 0;
    }

    public void run() {
        switch(this.mode.charAt(0)) {
            case 'p': // passif
                break;

            case 'a': //attack
                actualTarget.set.add(this);
                while(actualTarget.isAttacked.get() || actualTarget.set.peek()!=this) {
                    try {
                        System.out.println(this.id + " : WAIT");
                        synchronized (actualTarget.lockAttack) {
                            actualTarget.lockAttack.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                actualTarget.isAttacked.compareAndSet(false,true);
                try {
                    sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Random random = new Random();
                int rand = (int) (Math.random() * (100 - 50)) + 50;
                actualTarget.looseHealth(rand);
                actualTarget.isAttacked.compareAndSet(true,false);
                this.mode = "passif";
                player.server.getBroadcastOperations().sendEvent("attackFinished", new AttackObject(this.actualTarget.getId().toString(), this.id, player.getId().toString(), rand, 50));
                System.out.println(this.id+" : NOTIFYALL");
                actualTarget.set.remove(this);
                System.out.println(this.id + " : SET : "+actualTarget.set);
                synchronized (actualTarget.lockAttack) {
                     actualTarget.lockAttack.notifyAll();
                }
                break;

            case 'm': // mine
                while (player.isMining.get()) {
                    try {
                        synchronized(player.lockMine){
                            player.lockMine.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                player.isMining.compareAndSet(false,true);
                try {
                    Remote r = Naming.lookup("rmi://"+ InetAddress.getLocalHost().getHostAddress()+"/Mine/" + actualMineTarget);
                    if (r instanceof MineInterface) {
                        Integer gold = ((MineInterface) r).mineGold();
                        player.isMining.compareAndSet(true,false);
                        this.mode = "passif";
                        System.out.println("id de la mine où " + id + " mine : " + actualMineTarget);
                        player.winGold(gold);
                        player.server.getBroadcastOperations().sendEvent("mineFinished", new MiningObject(player.getId(), actualMineTarget, this.id, gold));
                    }
                } catch (RemoteException | InterruptedException | MalformedURLException | NotBoundException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                synchronized (player.lockMine){
                    player.lockMine.notifyAll();
                }
                break;
        }
    }

    public void setMode(String mode){
        this.mode = mode;
    }

    public void setActualTarget(Player idTarget){
        this.actualTarget = idTarget;
    }

    public String getMode() {
        return mode;
    }

    public String getId(){
        return this.id;
    }

    public void setActualMineTarget(int actualMineTarget) {
        this.actualMineTarget = actualMineTarget;
    }
}
